import Vue from "vue";
import App from "./App.vue";
import Home from "./components/home.vue";
import Top from "./components/top10.vue";
import VueRouter from 'vue-router';
import './assets/css/bootstrap.min.css';
import './assets/css/styles.css';

Vue.config.productionTip = false;

Vue.use(VueRouter)

const routes = [
  { path: '/', name: 'index', redirect: '/home', component: App },
  { path: '/home', name: 'home', component: Home },
  { path: '/top', name: 'top', component: Top },
]

const router = new VueRouter({
  routes
});

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
